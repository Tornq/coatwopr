﻿using UnityEngine;
using System.Collections;
using System.IO;
using TMPro;
using CoATwoPR;

public class DisplayHighestScore : MonoBehaviour
{
    //String that will be displayed when we have no highest score
    [SerializeField]
    private string noScoreText;
    //TextMesh that will display our highest score
    [SerializeField]
    private TextMeshProUGUI highestScoreTextMesh;
    //Int that is our loaded high score
    private int loadedHighScore;
    //Bool to check if a Json file with the saved score exsists
    private bool doesSavedScoreExsist;
    //Reference to our HighestScoreData Data class
    private HighestScoreData highestScoreData;
    //String to set the filepath of the json file
    private string filePath;

    //Awake is called when the script instance is being loaded
    void Awake()
    {
        //We set the filepath
        filePath = Application.persistentDataPath + "/Highestscore.json";
        //if the saved score exsists we call the LoadHighestScore method
        doesSavedScoreExsist = LoadHighestScore();
    }

    // Update is called once per frame
    void Update()
    {
        //If we have a saved score
        if (doesSavedScoreExsist)
        {
            //We update our TextMesh to display the highest score
            highestScoreTextMesh.text = loadedHighScore.ToString();
        }
        //If we dont have a saved score
        else
        {
            //We update our TextMesh to display the noScoreText text
            highestScoreTextMesh.text = noScoreText;
        }
    }

    //Method to load the highest score
    private bool LoadHighestScore()
    {
        //If a json file exsists in our filepath
        if (File.Exists(filePath))
        {
            //We read and load the highest score
            string loadedHighestScoreData = File.ReadAllText(filePath);
            highestScoreData = JsonUtility.FromJson<HighestScoreData>(loadedHighestScoreData);
            //we set the loadedHighScore to our loaded highest score
            loadedHighScore = highestScoreData.highestScore;
            //We return the method as true
            return true;
        }
        //If there is no Json file we return the method as false
        return false;
    }
}
