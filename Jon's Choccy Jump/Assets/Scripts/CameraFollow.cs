﻿using UnityEngine;
using System.Collections;
using CoATwoPR;

public class CameraFollow : MonoBehaviour
{
    //transform variable of the player
    public Transform player;
    //Offset to the player on x/y/z
    [SerializeField]
    private Vector3 offset;
    

    // LateUpdate is called once per frame after other Update functions
    void LateUpdate()
    {
        //transforms the position of the GO this script is attached to
        //To the player position, added by offset on x/y/z
        transform.position = player.position + offset;
    }
}
