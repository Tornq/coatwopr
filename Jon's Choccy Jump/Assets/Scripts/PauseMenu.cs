﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Sets the Namespace
namespace CoATwoPR
{
    public class PauseMenu : MonoBehaviour
    {
        //GameObject that stores our Pause Menu
        public GameObject pauseMenu;

        //Bool to check if the Menu is active
        public bool isMenuActive = false;

        // Update is called once per frame
        private void Update()
        {
            //If we hit esc
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                //If the Menu is already active
                if(isMenuActive)
                {
                    //We call our resume method
                    Resume();
                }
                //If the Menu isnt active
                else
                {
                    //We call our pause method
                    Pause();
                }

            }

        }

        //Method to pause the game
        public void Pause()
        {
            //Activates the pauseMenu GO
            pauseMenu.SetActive(true);
            //Sets the bool to true
            isMenuActive = true;
            //pauses the time
            Time.timeScale = 0f;
        }

        //Method to resume the game
        public void Resume()
        {
            //Deactivates the pauseMenu GO
            pauseMenu.SetActive(false);
            //Sets the bool to false
            isMenuActive = false;
            //continues the time normally
            Time.timeScale = 1f;
        }
    }
}

