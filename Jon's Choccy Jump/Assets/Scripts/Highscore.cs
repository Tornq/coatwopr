﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using TMPro;

public class Highscore : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI textMesh;
    private int currentScore;

    // Update is called once per frame
    void Update()
    {
        textMesh.text = currentScore.ToString();
    }

    public void AddScore(int score)
    {
        currentScore += score;
    }

    public void SaveScore()
    {
        string filepath = Application.persistentDataPath + "/Highscore.json";
        HighScoreData highscoreData = new HighScoreData();
        highscoreData.highScore = currentScore;
        string dataToSave = JsonUtility.ToJson(highscoreData);
        File.WriteAllText(filepath, dataToSave);
    }

    public int CurrentScore
    {
        get { return currentScore; }
        set { currentScore = value; }
    }
}
