﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CoATwoPR;

public class PlayerMovement : MonoBehaviour
{
    //Rigidbody of the GO this script is attached to
    private Rigidbody2D newRigidbody;
    //Float to store our speed
    [SerializeField]
    private float speed;
    //Float to store our jump Force
    [SerializeField]
    private float jumpForce;
    //Float to store our move input
    private float moveInput;

    //Bool to check if the player is grounded
    private bool isGrounded;
    //Transform that sets the feet position of the player
    [SerializeField]
    private Transform feetPosition;
    //Float to check the radius around the feet position
    [SerializeField]
    private float checkRadius;
    //Layermask to detect the ground
    [SerializeField]
    private LayerMask ground;

    //Float to count our jump time
    private float jumpTimeCounter;
    //Float to store our jump time
    [SerializeField]
    private float jumpTime;
    //Bool to check if we are jumping
    private bool isJumping;

    //Reference to our AdditiveHighScore script
    [SerializeField]
    private AdditiveHighscore highscore;

    //Float to set the left edge bounds
    [SerializeField]
    private float leftEdge;
    //Float to set the right edge bounds
    [SerializeField]
    private float rightEdge;

    //GameObject that stores our dialogue
    [SerializeField]
    private GameObject dialogueUI;

    //Refrence to the animator
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        //Gets the Animator component of the GO this script is attached to
        anim = GetComponent<Animator>();
        //Gets the Rigidbody component of the GO this script is attached to
        newRigidbody = GetComponent<Rigidbody2D>();
    }

    //FixedUpdate can be called multiple times per frame and is independent from the frame rate
    private void FixedUpdate()
    {
        //Sets the moveInput to our Horizontal axis (x)
        moveInput = Input.GetAxisRaw("Horizontal");
        //Lets the player move along x(moveInput) times our speed
        //And along y dependant on our velocity
        newRigidbody.velocity = new Vector2(moveInput * speed, newRigidbody.velocity.y);
    }

    // Update is called once per frame
    void Update()
    {
        //The player is grounded if the feet position is on the ground
        //The ground gets detected by the radius we set in the checkRadius float
        isGrounded = Physics2D.OverlapCircle(feetPosition.position, checkRadius, ground);

        //If we have no move input
        if (moveInput == 0)
        {
            //We set the running animation to false
            //The idle animation will get played
            anim.SetBool("isRunning", false);
        }
        //If we have move input
        else
        {
            //We set the running animation to true
            //The running animation will get played
            anim.SetBool("isRunning", true);
        }

        //If the moveInput is greater than 0 (Move to the right)
        if (moveInput > 0)
        {
            //We get our spriterenderer component of the GO this script is attached to
            GetComponent<SpriteRenderer>();
            //We transform the position to 0x 0y 0z
            //The character will look to the right
            transform.localEulerAngles = new Vector3(0, 0, 0);
        }
        //If the moveInput is lower than 0 (Move to the left)
        else if(moveInput < 0)
        {
            //We flip the spriterenderer by 0x 180y 0z
            //The character will look to the left
            transform.localEulerAngles = new Vector3(0, 180, 0);
        }

        //If the player is grounded and we press space
        if(isGrounded == true && Input.GetKeyDown(KeyCode.Space))
        {
            //The take off animation will get played
            anim.SetTrigger("takeOff");
            //We set the bool to true
            isJumping = true;
            //We set the jumpTimeCounter to our jumpTime
            jumpTimeCounter = jumpTime;
            //We let the GO jump up multiplied by our jumpforce 
            //which will set how high we can jump
            newRigidbody.velocity = Vector2.up * jumpForce;
        }

        //If the player is grounded
        if (isGrounded == true)
        {
            //We set the jumping animation bool to false
            anim.SetBool("isJumping", false);
        }
        //If the player is not grounded
        else
        {
            //We set the jumping animation bool to true
            //The jumping animation will get played
            anim.SetBool("isJumping", true);
        }

        //If space is being pressed and we are jumping
        if(Input.GetKey(KeyCode.Space) && isJumping == true)
        {
            //if the jumpTimeCounter is greater than 0
            if(jumpTimeCounter > 0)
            {
                // We let the GO jump up multiplied by our jumpforce
                //which will set how high we can jump
                newRigidbody.velocity = Vector2.up * jumpForce;
                //Counts down the jumpTime along deltaTime
                jumpTimeCounter -= Time.deltaTime;
            }
            //if the jumpTimeCounter is equal or less than 0
            else
            {
                //We set the isJumping bool to false
                isJumping = false;
            }
        }
        //If we let space go
        if(Input.GetKeyUp(KeyCode.Space))
        {
            //We set the isJumping bool to false
            isJumping = false;
        }

        //A vector 3 that is set to the transform position of the GO this script is attached to
        Vector3 cachedPosition = transform.position;
        //sets the cachedPosition along x to a clamed position on the X axis
        //This will limit the players movement so that we can only move between left and right edge
        //and not go beyond those points
        cachedPosition.x = Mathf.Clamp(cachedPosition.x, leftEdge, rightEdge);

        //Sets the position of the GO to the chachedPosition
        transform.position = cachedPosition;
    }

    //Method when the GO this script is attached to enters a collider
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //If the collided GO is tagged with "Milk"
        if(collision.gameObject.CompareTag("Milk"))
        {
            //We destroy the GO we collided with
            Destroy(collision.gameObject);
            //And add one point to our score with the help of our highscore reference
            highscore.AddScore(1);
            
        }

        //If the collided GO is tagged with "Obst" (Obstacles)
        if(collision.gameObject.CompareTag("Obst"))
        {
            //We set the speed to 3
            speed = 3;
            //the jumpf force to 0 so that we cant jump
            jumpForce = 0f;
            //isGrounded to true as we cant leave the ground
            isGrounded = true;
            //and isJumping to false as we cant jump
            isJumping = false;
        }

        //If the collided GO is tagged with "Dia" (Dialogue)
        if (collision.gameObject.CompareTag("Dia"))
        {
            //We set the dialogueUI GO to active
            dialogueUI.SetActive(true);
            //And get the dialogueTrigger component of the collided GO
            //To trigger the dialogue
            collision.gameObject.GetComponent<DialogueTrigger>().TriggerDialogue();
        }
    }
    //Method when the GO this script is attached to exits a collider
    private void OnTriggerExit2D(Collider2D collision)
    {
        //If the collided GO is tagged with "Obst" (Obstacles)
        if (collision.gameObject.CompareTag("Obst"))
        {
            //We set the speed back to 8, actively overwriting any buffs we might have gotten
            speed = 8;
            //We set the jumpForce back to 3.5, actively overwriting any buffs we might have gotten
            jumpForce = 3.5f;
        }

        //If the collided GO is tagged with "Dia" (Dialogue)
        if (collision.gameObject.CompareTag("Dia"))
        {
            //We deactivate the DialogueUI GO
            dialogueUI.SetActive(false);
        }
    }

    //Property of our speed so other scripts can access it
    public float Speed
    {
        get { return speed; }
        set { speed = value; }
    }

    //Property of our jumpForce so other scripts can access it
    public float JumpForce
    {
        get { return jumpForce; }
        set { jumpForce = value; }
    }
}
