﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using CoATwoPR;

public class Timer : MonoBehaviour
{
    //Float to set the current time
    public float currentTime;
    //Float to set the starting time
    [SerializeField]
    private float startingTime;

    //Reference to our countdown script
    [SerializeField]
    private Countdown countdown;
    
    //TextMesh to display the countdown
    [SerializeField]
    private TextMeshProUGUI countdownText;

    //String to set the name of the scene that should be loaded
    [SerializeField]
    private string sceneName;

    //Refrence to our AdditiveHighScore script
    [SerializeField]
    private AdditiveHighscore highscore;

    //Reference to our HuxleyTweenFly script
    public HuxleyTweenFly huxleyTweenFly;
    //Reference to our HuxleyTweenSit script
    public HuxleyTweenSit huxleyTweenSit;
    //GameObject that has the huxleyFlying sprite
    [SerializeField]
    private GameObject huxleyFly;
    //GameObject that is a black screen
    [SerializeField]
    private GameObject blackScreen;

    // Start is called before the first frame update
    void Start()
    {
        //Sets the current time to the starting time
        currentTime = startingTime;
    }

    // Update is called once per frame
    void Update()
    {
        //If we have a countdown assigned
        if(countdown != null)
        {
            //If the countdown getReadyTime is equal or less than 0
            if (countdown.getReadyTime <= 0)
            {
                //We count down the current time by delta time
                currentTime -= Time.deltaTime;
            }
        }
        //If we dont have a countdown assigned
        else
        {
            //We count down the current time by delta time
            currentTime -= Time.deltaTime;
        }
        
        //We update our TextMesh to display the current time by 2 decimals
        countdownText.text = currentTime.ToString("0.00");
        //If the current time is equal or less than 5
        if(currentTime <= 5f)
        {
            //If we have a huxleyTweenSit assigned
            if(huxleyTweenSit != null)
            {
                //We play the tween
                huxleyTweenSit.PlayTween();
            }
        }

        //If the current time is equal or less than 0
        if(currentTime <= 0f)
        {
            //If we have a huxleyTweenFly and huxleyFly GO assigned
            if(huxleyTweenFly != null && huxleyFly != null)
            {
                //We set the countdown text to 0
                countdownText.text = "0";
                //Activate the huxleyFly GO
                huxleyFly.SetActive(true);
                //And play the Tween
                huxleyTweenFly.PlayTween();
            }
        }

        //If the current time is equal or less than -3
        if (currentTime <= -3f)
        {
            //We save the current score
            highscore.SaveScore();
            //We check if the current score is higher than the highest score
            //If so we overwrite and save it
            highscore.CheckHighestScore();
            //We activate the blackScreen GO
            blackScreen.SetActive(true);
            //We load the scene by name
            SceneManager.LoadScene(sceneName);
        }
    }
}
