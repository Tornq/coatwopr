﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using CoATwoPR;

public class SceneLoader : MonoBehaviour
{
    //String to determine the scene name which should be loaded
    [SerializeField]
    private string sceneName;

    //Reference to our AdditiveHighScore script
    public AdditiveHighscore additiveHighscore;

    //Method to load scenes
    public void LoadScene()
    {
        //Loads the scene by name
        SceneManager.LoadScene(sceneName);
    }

    //Method to exit the game
    public void ExitGame()
    {
        //Quits the game
        Application.Quit();
    }

    //Method to reset our highscore
    public void ResetScore()
    {
        //Sets the CurrentScore of our additiveHighscore script to 0
        additiveHighscore.CurrentScore = 0;
        //Saves the highsscore
        additiveHighscore.SaveScore();
    }

    //Method to save the score
    public void SaveScore()
    {
        //Saved the highscore
        additiveHighscore.SaveScore();
    }

    //Method to save the highest score
    public void SaveHighestScore()
    {
        //Checks if the highscore is higher than the highest score
        //If so it will overwrite the value and save it
        additiveHighscore.CheckHighestScore();
    }
}
