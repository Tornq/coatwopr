﻿using UnityEngine;
using System.Collections;
using CoATwoPR;

//Data class to store the Dialogue
[System.Serializable]
public class Dialogue
{
    //string to set the name
    public string name;
    //Lets us have at least 3 lines of text and a max of 10
    [TextArea(3,10)]
    //Array of sentences
    public string[] sentences;
}
