﻿using UnityEngine;
using System.Collections;
using System.IO;
using TMPro;
using CoATwoPR;

public class DisplayHighScore : MonoBehaviour
{
    //String that will be displayed when we have no highscore
    [SerializeField]
    private string noScoreText;
    //TextMesh that will display our highscore
    [SerializeField]
    private TextMeshProUGUI highScoreTextMesh;
    //Int that is our loaded high score
    private int loadedHighScore;
    //Bool to check if a Json file with the saved score exsists
    private bool doesSavedScoreExsist;
    //Reference to our HighScoreData Data class
    private HighScoreData highScoreData;
    //String to set the filepath of the json file
    private string filePath;

    //Awake is called when the script instance is being loaded
    void Awake()
    {
        //We set the filepath
        filePath = Application.persistentDataPath + "/Highscore.json";
        //if the saved score exsists we call the LoadHighScore method
        doesSavedScoreExsist = LoadHighScore();
    }

    // Update is called once per frame
    void Update()
    {
        //If we have a saved score
        if (doesSavedScoreExsist)
        {
            //We update our TextMesh to display the highscore
            highScoreTextMesh.text = loadedHighScore.ToString();
        }
        //If we dont have a saved score
        else
        {
            //We update our TextMesh to display the noScoreText text
            highScoreTextMesh.text = noScoreText;
        }
    }

    //Method to load the highscore
    private bool LoadHighScore()
    {
        //If a json file exsists in our filepath
        if (File.Exists(filePath))
        {
            //We read and load the highscore
            string loadedHighScoreData = File.ReadAllText(filePath);
            highScoreData = JsonUtility.FromJson<HighScoreData>(loadedHighScoreData);
            //we set the loadedHighScore to our loaded highscore
            loadedHighScore = highScoreData.highScore;
            //We return the method as true
            return true;
        }
        //If there is no Json file we return the method as false
        return false;
    }
}
