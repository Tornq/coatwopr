﻿using UnityEngine;
using System.Collections;
using System.IO;
using TMPro;
using CoATwoPR;

public class AdditiveHighscore : MonoBehaviour
{
    //String to display text when there is no score
    [SerializeField]
    private string noScoreText;
    //textMesh to display the score in game
    [SerializeField]
    private TextMeshProUGUI textMesh;

    //The highscore that was loaded from the json file
    private int loadedHighScore;
    //The highest score that was loaded from the json file
    private int loadedHighestScore;

    //Bool to check if a json file with the highscore exsists
    private bool doesSavedScoreExsist;
    //Bool to check if a json file with the highest score exsists
    private bool doesHighestScoreExist;

    //Reference to the HighscoreData class
    private HighScoreData highScoreData;
    //Reference to the HighestScoreData class
    private HighestScoreData highestScoreData;

    //File path to determine where we will save the score
    private string filePath;
    //File path to determine where we will save the highest score
    private string filePathHigh;

    //Up to date in-game score
    private int currentScore;

    //Awake is called when the script instance is being loaded
    void Awake()
    {
        //sets the filepath where the highscore is saved
        filePath = Application.persistentDataPath + "/Highscore.json";
        //If a saved score exsists we load it
        doesSavedScoreExsist = LoadHighScore();

        //sets the filepath where the highest score is saved
        filePathHigh = Application.persistentDataPath + "/Highestscore.json";
        //If a saved highest score exsists we load it
        doesHighestScoreExist = LoadHighestScore();
        
    }

    //Start is called at the first frame
    private void Start()
    {
        //We set the current in game score to the highscore that we loaded
        currentScore = loadedHighScore;
    }

    // Update is called once per frame
    void Update()
    {
        //we update the text to always display the current score
        textMesh.text = currentScore.ToString();
    }

    //method to increase our score
    public void AddScore(int score)
    {
        //Adds to the current score a int value
        currentScore += score;
    }

    //Checks if the current score beats the previous highscore
    public void CheckHighestScore()
    {
        if(currentScore >= loadedHighestScore)
        {
            //If the current score is higher we overwrite the highscore to it 
            //by calling the SaveHighestScore method
            SaveHighestScore();
        }
    }

    //Loads the score
    private bool LoadHighScore()
    {
        //Executes if there is a json file with the highscore
        if (File.Exists(filePath))
        {
            //Reads and sets the previously saved highscore data
            string loadedHighScoreData = File.ReadAllText(filePath);
            highScoreData = JsonUtility.FromJson<HighScoreData>(loadedHighScoreData);
            //Sets loadedHighScore to the loaded highscore data
            loadedHighScore = highScoreData.highScore;
            //We return the methos as true
            return true;
        }
        //If the file doesnt exsist we return the method as false
        return false;
    }

    //Loads the highest score
    private bool LoadHighestScore()
    {
        //Executes if there is a json file with the highest score
        if (File.Exists(filePathHigh))
        {
            string loadedHighestScoreData = File.ReadAllText(filePathHigh);
            highestScoreData = JsonUtility.FromJson<HighestScoreData>(loadedHighestScoreData);
            //Sets loadedHighestScore to the loaded highest score data
            loadedHighestScore = highestScoreData.highestScore;
            //We return the methos as true
            return true;
        }
        //If the file doesnt exsist we return the method as false
        return false;
    }

    //Method to save the score
    public void SaveScore()
    {
        //Saves the score to a set filepath
        string filepath = Application.persistentDataPath + "/Highscore.json";
        HighScoreData highscoreData = new HighScoreData();
        highscoreData.highScore = currentScore;
        string dataToSave = JsonUtility.ToJson(highscoreData);
        File.WriteAllText(filepath, dataToSave);
    }

    //Method to save the highest score
    private void SaveHighestScore()
    {
        //Saves the highest score to a set filepath
        string filepath = Application.persistentDataPath + "/Highestscore.json";
        HighestScoreData highestscoreData = new HighestScoreData();
        highestscoreData.highestScore = currentScore;
        string dataToSave = JsonUtility.ToJson(highestscoreData);
        File.WriteAllText(filepath, dataToSave);
    }

    //Property of our current score so other scripts can access it
    public int CurrentScore
    {
        get { return currentScore; }
        set { currentScore = value; }
    }
}
