﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using CoATwoPR;

public class HuxleyTweenFly : MonoBehaviour
{
    //Float for our end position
    public float endPosition;
    //Float for how long it will take to play the tween
    public float tweenDuration;
  
    //Method to play the tween
    public void PlayTween()
    {
        //DoTween tranformation along the X axis with the parameters endposition and duration
        //Once the tween is finished we call our Finished method
            transform.DOLocalMoveX(endPosition, tweenDuration).OnComplete(Finished);
    }

    //Method to deactivate the GO
    private void Finished()
    {
        //disables the GO this script is attached to
        gameObject.SetActive(false);
    }
}
