﻿using UnityEngine;
using System.Collections;
using CoATwoPR;

public class PowerUpSpeed : MonoBehaviour
{
    //Reference to our PlayerMovement script
    [SerializeField]
    private PlayerMovement playerMovement;

    //If we collide with something
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //And the GO we collide with is tagged with "Player"
        if (collision.gameObject.CompareTag("Player"))
        {
            //We set the Speed in our PlayerMovement script to 15;
            playerMovement.Speed = 15;
            //We destroy the GO this script is attached to
            Destroy(gameObject);
        }
    }
}
