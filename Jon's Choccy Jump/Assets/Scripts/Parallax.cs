﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CoATwoPR;

public class Parallax : MonoBehaviour
{
    //Float that stores the length of the GO
    private float length;
    //Float that stores the start position of the GO
    private float startposition;
    //GameObject that is our mainCamera
    //We switched it to our player in the inspector as the camera follows the player
    //Via late update, resulting in a glitchy background
    public GameObject mainCamera;
    //Float that sets the parallax effect
    public float parallaxEffect;

    // Start is called before the first frame update
    void Start()
    {
        //Sets the start position to the X position of the GO
        startposition = transform.position.x;
        //Sets the length to the bounds of the Image
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    // Update is called once per frame
    void Update()
    {
        //sets the distance to the transform position of the mainCamera(Player) 
        //Multiplied by the float parallaxEffect
        float distance = (mainCamera.transform.position.x * parallaxEffect);
        //Transforms the position of the GO by adding the float distance (x position) to the start position
        //Doesnt transform y or z as we didnt set it
        transform.position = new Vector3(startposition + distance, transform.position.y, transform.position.z);
    }
}
