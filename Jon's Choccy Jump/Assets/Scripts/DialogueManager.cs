﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using CoATwoPR;

public class DialogueManager : MonoBehaviour
{
    //GameObject that hosts our dialogue
    public GameObject dialogueUI;
    //TextMesh that will display the name
    public TextMeshProUGUI nameText;
    //TextMesh that will display the dialogue
    public TextMeshProUGUI dialogueText;

    //A queue for our sentences
    private Queue<string> sentences;

    // Start is called before the first frame update
    void Start()
    {
        //sets our sentences into a new Queue
        sentences = new Queue<string>();
    }

    //Method to start the dialogue with a reference to our Dialogue data class
    public void StartDialogue (Dialogue dialogue)
    {
        //displays the name in our name TextMesh
        nameText.text = dialogue.name;

        //Clears all previously queued sentences
        sentences.Clear();

        //for each sentence in our Dialogue data class
        foreach (string sentence in dialogue.sentences)
        {
            //We enqueue them in our Queue
            sentences.Enqueue(sentence);
        }

        //We call the method to display the next sentence
        DisplayNextSentence();
    }

    //Method to display the next sentence
    public void DisplayNextSentence()
    {
        //If we have no sentences
        if(sentences.Count == 0)
        {
            //We deactive our dialogue GO
            dialogueUI.SetActive(false);
            //we return our method
            return;
        }
        //Deques the sentence
        string sentence = sentences.Dequeue();
        //displays the dequeued sentence
        dialogueText.text = sentence;
    }
}
