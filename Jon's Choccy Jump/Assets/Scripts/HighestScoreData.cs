﻿using UnityEngine;
using UnityEditor;
using System;
using CoATwoPR;

//Data class to store the HighestScoreData
[SerializeField]
public class HighestScoreData
{
    //Int to store the highest score
    public int highestScore;
}