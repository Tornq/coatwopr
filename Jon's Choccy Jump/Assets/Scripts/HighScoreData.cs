﻿using UnityEngine;
using UnityEditor;
using System;
using CoATwoPR;

//Data class to store the highscore
[SerializeField]
public class HighScoreData
{
    //Int to store the highscore
    public int highScore;
}