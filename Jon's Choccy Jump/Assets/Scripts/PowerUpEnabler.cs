﻿using UnityEngine;
using System.Collections;
using CoATwoPR;

public class PowerUpEnabler : MonoBehaviour
{
    //Reference to our AdditiveHighscore script
    [SerializeField]
    private AdditiveHighscore highscore;
    //GameObject that is our Powerup
    [SerializeField]
    private GameObject powerup;
    //Int to determine the halfscore of our highscore
    //This can be set to any number and doesnt have to be the half score
    //We intended to use it as half score
    //But ended up setting it to a number that we found reasonable
    [SerializeField]
    private int halfScore;

    // Update is called once per frame
    void Update()
    {
        //if the current score is equal or higher than the halfScore
        if(highscore.CurrentScore >= halfScore)
        {
            //If we have a poweup assigned
            if(powerup != null)
            {
                //we activate the powerup GO
                powerup.SetActive(true);
            }
        }
    }

}
