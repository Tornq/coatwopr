﻿using UnityEngine;
using System.Collections;
using CoATwoPR;

public class PowerUpInvincible : MonoBehaviour
{
    //GameObject that are our obstacles
    //We have a gameobject in the inspector where all obstacles of the level
    //are inside as children
    [SerializeField]
    private GameObject obstacles;

    //If we collide with something
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //And the GO we collide with is tagged with "Player"
        if (collision.gameObject.CompareTag("Player"))
        {
            //We deactivate the obstacles
            obstacles.SetActive(false);
            //We destroy the GO this script is attached to
            Destroy(gameObject);
        }
    }
}
