﻿using UnityEngine;
using UnityEditor;
using CoATwoPR;

public class DialogueTrigger : MonoBehaviour
{
    //Reference to our Dialogue Data class
    public Dialogue dialogue;
    //GameObject that is our textbox for Vexx
    [SerializeField]
    private GameObject vexxTextBox;
    //GameObject that is our textbox for Huxley
    [SerializeField]
    private GameObject huxleyTextBox;

    //Method to trigger the dialogue
    public void TriggerDialogue()
    {
        //Finds the DialogueManager in the scene and starts the dialogue
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue);

        //If the name set in our Dialogue data class is Vexx
        if( dialogue.name == "Vexx")
        {
            //We activate the Vexx textbox
            vexxTextBox.SetActive(true);
            //And deactivate the Huxley text box
            huxleyTextBox.SetActive(false);
        }
        //If the name set in our Dialogue data class is Huxley
        if (dialogue.name == "Huxley")
        {
            //We activate the Huxley text box
            huxleyTextBox.SetActive(true);
            //And deactivate the Vexx text box
            vexxTextBox.SetActive(false);
        }
    }
}