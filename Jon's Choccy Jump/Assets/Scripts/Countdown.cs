﻿using UnityEngine;
using System.Collections;
using TMPro;
using CoATwoPR;

public class Countdown : MonoBehaviour
{
    //float to set the get ready time
    public float getReadyTime;
    //float to initialise the get ready time
    [SerializeField]
    private float getReadyStartingTime;

    //Text mesh to display the get ready time
    [SerializeField]
    private TextMeshProUGUI getReadyText;

    //reference to our PlayerMovement script
    [SerializeField]
    private PlayerMovement playerMovement;

    //Start is called on the first frame
    private void Start()
    {
        //If we have a PlayerMovement in the scene
        if(playerMovement != null)
        {
            //We set the speed of it to 0
            playerMovement.Speed = 0f;
        }
        //Initializes the get ready time to our starting time
        getReadyTime = getReadyStartingTime;
    }

    //Update is called once per frame
    private void Update()
    {
        //Counts down the get ready time by delta time
        getReadyTime -= Time.deltaTime;
        //Updates the text to always display the get ready time
        //With no decimals
        getReadyText.text = getReadyTime.ToString("0");

        //When the get ready time is equal or below 0
        if (getReadyTime <= 0f)
        {
            //We destroy the GO this script is attached to
            Destroy(this.gameObject);
            //If we have a PlayerMovement in the scene
            if(playerMovement != null)
            {
                //We set the speed back to 8
                playerMovement.Speed = 8f;
            }
        }
        //If we dont have a GO with the Timer tag
        if(GameObject.Find("Timer") == null)
        {
            //We let the time pass normally
            Time.timeScale = 1f;
        }
    }
}
