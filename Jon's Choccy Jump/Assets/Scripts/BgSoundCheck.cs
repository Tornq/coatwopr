﻿using UnityEngine;
using UnityEditor;
using CoATwoPR;

public class BgSoundCheck : MonoBehaviour
{
    //Gameobject that is our audioSource
    public GameObject audioSource;
    //Start is called on the first frame
    private void Start()
    {
        //If an audiosource GO is in the scene...
        if(audioSource != null)
        {
            //...we destroy our BGSound GO by finding it with the Audio Tag
            Destroy(GameObject.FindWithTag("Audio"));
        }
    }

}